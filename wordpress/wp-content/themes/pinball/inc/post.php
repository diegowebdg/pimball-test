<?php
$category = get_the_category();
?>
<article class="post">
  <a href="<?php the_permalink(); ?>" class="post__wrapper">
    <div class="post__image-wrapper">
      <?php the_post_thumbnail( 'post_thumbs', [ 'class' => 'post__image' ] ) ?>
    </div>
    <div class="post__info-wrapper">
      <h1 class="post__title"><?php the_title(); ?></h1>
      <span class="post__category"><?= $category[0]->name ?></span>
      <p class="post__excerpt"><?= excerpt(25); ?></p>
    </div>
    <div class="post__rating-social-wrapper">
      <div class="post__rating-wrapper">
        <i class="post__star post__star--full"></i>
        <i class="post__star post__star--full"></i>
        <i class="post__star post__star--empty"></i>
        <i class="post__star post__star--empty"></i>
      </div>
      <div class="post__social-wrapper">
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo rawurlencode(get_the_permalink()); ?>" class="post__share post__share--facebook share">Compartilhe no Facebook</a>
      </div>
    </div>
  </a>
</article>
