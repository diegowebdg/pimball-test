<!DOCTYPE html>
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/style.css">
  <meta property="og:image" content="<?php bloginfo('template_directory'); ?>/assets/img/share-image.jpg">
  <link rel="apple-touch-icon" sizes="57x57" href="http://cdn.videocamp.com/images/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="http://cdn.videocamp.com/images/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="http://cdn.videocamp.com/images/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="http://cdn.videocamp.com/images/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="http://cdn.videocamp.com/images/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="http://cdn.videocamp.com/images/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="http://cdn.videocamp.com/images/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="http://cdn.videocamp.com/images/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="http://cdn.videocamp.com/images/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="http://cdn.videocamp.com/images/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="http://cdn.videocamp.com/images/android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="http://cdn.videocamp.com/images/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="http://cdn.videocamp.com/images/favicon-16x16.png" sizes="16x16">
  <meta name="msapplication-TileColor" content="#14003c">
  <meta name="theme-color" content="#14003c">
	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
  <header class="header">
    <div class="container">
      <div class="header__logo-wrapper">
        <?= (is_home()) ? '<h1 class="header__logo">' : '<a href="'.home_url('/').'" title="Voltar para a home" class="header__logo">'; ?>Pinball | The grid theme<?= (is_home()) ? '</h1>' : '</a>'; ?>
      </div>
      <div class="header__central">
        <span class="header__menu-opener"><i></i><i></i><i></i></span>
        <?= (is_home()) ? '<h1 class="header__logo header__logo--mobile">' : '<a href="'.home_url('/').'" title="Voltar para a home" class="header__logo header__logo--mobile">'; ?>Pinball | The grid theme<?= (is_home()) ? '</h1>' : '</a>'; ?>
        <div class="header__search-bar-wrapper">
          <input type="search" class="header__search-input">
          <span class="header__search-icon"></span>
        </div>
      </div>
      <div class="header__user-wrapper">
        <div class="header__user-image-wrapper">
          <img src="http://placehold.it/38x38" alt="" class="header__user-image">
        </div>
        <span class="header__user-name">Welcome John</span>
      </div>
    </div>
  </header>
  <div class="main">
