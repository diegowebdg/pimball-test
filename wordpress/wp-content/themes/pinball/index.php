<?php
get_header();
?>
<main class="post-list">
  <div class="container">
    <div class="post-list__list">
      <?php
      $posts_args = array(
        'posts_per_page' => -1
      );
      $posts_query = new WP_Query($posts_args);
      if( $posts_query->have_posts()): while($posts_query->have_posts()): $posts_query->the_post();
        include(locate_template('inc/post.php'));
      endwhile; endif;
      ?>
    </div>
  </div>
</main>
<?php
get_footer();
?>
