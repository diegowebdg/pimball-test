<?php
get_header();
the_post();
?>
<section class="single-content">
  <div class="container">
    <h1><?php the_title(); ?></h1>
    <?php the_content(); ?>
  </div>
</section>
<?php
get_footer();
?>
