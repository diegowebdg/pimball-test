const gulp = require('gulp');

const babelify = require('babelify');
const browserify = require('browserify');
const uglify = require('gulp-uglify');
const notify = require('gulp-notify');

const concat = require('gulp-concat');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const util = require('gulp-util');

const livereload = require('gulp-livereload');
const plumber = require('gulp-plumber');
const projectFolder = './wordpress/wp-content/themes/pinball';

gulp.task('scripts_app', () => {
  var bundler = browserify({
    entries: ['./src/js/App.js']
  }).transform(
    babelify.configure({
      presets: ['es2015']
    })
  );

  var bundle = () => {
    return bundler
      .bundle()
      .pipe(source('app.js'))
      .pipe(plumber())
      .pipe(buffer())
      .pipe(global.isWatching ? util.noop() : uglify())
      .pipe(gulp.dest(projectFolder + '/assets/js/'));
  };

  return bundle();
});

gulp.task('scripts_vendors', () => {
  var scripts_vendors = ['./src/vendors/**/*.js'];

  return gulp
    .src(scripts_vendors)
    .pipe(plumber())
    .pipe(concat('app_vendors.js'))
    .pipe(gulp.dest(projectFolder + '/assets/js/'));
});

gulp.task('scripts', ['scripts_app', 'scripts_vendors'], () => {
  var files = [
    projectFolder + '/assets/js/app_vendors.js',
    projectFolder + '/assets/js/app.js'
  ];

  return gulp
    .src(files)
    .pipe(plumber())
    .pipe(concat('scripts.combined.js'))
    .pipe(gulp.dest('./wordpress/wp-content/themes/pinball/assets/js/'))
    .pipe(global.isWatching ? notify('Scripts combined!') : util.noop())
    .pipe(livereload());
});
