const gulp = require('gulp');
const stylus = require('gulp-stylus');
const nib = require('nib');
const notify = require('gulp-notify');
const util = require('gulp-util');
const concat = require('gulp-concat');
const livereload = require('gulp-livereload');
const plumber = require('gulp-plumber');
const projectFolder = './wordpress/wp-content/themes/pinball';

gulp.task('styles_app', () => {
  return gulp
    .src('./src/styl/style.styl')
    .pipe(plumber())
    .pipe(
      stylus({
        compress: true,
        use: nib()
      })
    )
    .pipe(gulp.dest(projectFolder + '/'));
});

gulp.task('styles_vendors', () => {
  var vendors = './src/vendors/**/*.css';

  return gulp
    .src(vendors)
    .pipe(plumber())
    .pipe(concat('style_vendors.css'))
    .pipe(gulp.dest(projectFolder + '/'));
});

gulp.task('styles', ['styles_app', 'styles_vendors'], () => {
  var files = [
    projectFolder + '/style_vendors.css',
    projectFolder + '/style.css'
  ];

  return gulp
    .src(files)
    .pipe(plumber())
    .pipe(concat('style.css'))
    .pipe(gulp.dest(projectFolder + '/'))
    .pipe(global.isWatching ? notify('Styles combined!') : util.noop())
    .pipe(livereload());
});
