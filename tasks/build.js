const gulp = require('gulp')
const sequence = require('run-sequence')

gulp.task('build', function() {
	sequence('sprites', 'styles', 'scripts', 'clean');
});