const gulp = require('gulp');
const util = require('gulp-util');
const spritesmith = require('gulp.spritesmith');
const livereload = require('gulp-livereload');
const projectFolder = './wordpress/wp-content/themes/pinball';

gulp.task('sprites', function() {
  var spriteData = gulp.src('./src/img/*.png').pipe(
    spritesmith({
      cssFormat: 'stylus',
      algorithm: 'binary-tree',
      padding: 2,
      imgPath: 'assets/img/sprite.png',
      imgName: 'sprite.png',
      cssName: 'sprite.styl',
      retinaImgName: 'sprite@2x.png',
      retinaSrcFilter: ['./src/img/*@2x.png']
    })
  );

  spriteData.img.pipe(gulp.dest(projectFolder + '/assets/img/'));
  spriteData.css.pipe(gulp.dest('./src/styl/utils/'));

  return spriteData;
});
