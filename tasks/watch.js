const gulp = require('gulp');
const watch = require('gulp-watch');
const livereload = require('gulp-livereload');
const sequence = require('run-sequence').use(gulp);
const projectFolder = './wordpress/wp-content/themes/pinball';

gulp.task('watch', ['build'], function() {
  global.isWatching = true;

  livereload.listen();

  gulp.watch('./src/img/*.png', () => {
    sequence('sprites', 'styles', 'clean-css');
  });

  gulp.watch(['./src/styl/**/*.styl', './src/vendors/**/*.css'], () => {
    sequence('styles', 'clean-css');
  });

  gulp.watch(['./src/js/**/*.js', './src/vendors/**/*.js'], () => {
    sequence('scripts', 'clean-js');
  });
});
