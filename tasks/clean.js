const gulp = require('gulp');
const clean = require('gulp-clean');
const projectFolder = './wordpress/wp-content/themes/pinball';

gulp.task('clean', function() {
  var files = [
    projectFolder + '/assets/js/app.js',
    projectFolder + '/assets/js/app_vendors.js',
    projectFolder + '/style_vendors.css'
  ];
  return gulp.src(files, { read: false }).pipe(clean());
});

gulp.task('clean-css', function() {
  return gulp
    .src(projectFolder + '/style_vendors.css', { read: false })
    .pipe(clean());
});

gulp.task('clean-js', function() {
  var files = [
    projectFolder + '/assets/js/app.js',
    projectFolder + '/assets/js/app_vendors.js'
  ];
  return gulp.src(files, { read: false }).pipe(clean());
});
