/*jshint esversion: 6 */
import Main from './modules/Main';

((document, window) => {
  window.APP = {
    Main: new Main(),
    View: null,
    Modules: []
  };
  APP.Main.init();
})(document, window);

export default Main;
