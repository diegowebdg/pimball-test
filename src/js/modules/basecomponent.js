/*jshint esversion: 6 */
class BaseComponent {
  constructor() {}
  init() {
    this.attachEvents();
    this.render();
  }
  scroll(windowScrollTop){}
  resize(width, height) {}
  render() {}
}
export default BaseComponent;
