/*jshint esversion: 6 */
import Home from './Home';

class Main {
  constructor() {
    this.body = document.body;
  }

  init() {
    this.resize();

    if (this.body.classList.contains('home')) {
      this.Home = new Home();
      APP.Modules.push(this.Home);
    }

    this.modulesInitialize();
    this.attachEvents();
  }

  attachEvents() {}

  //Common
  resize() {
    this.width =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      this.body.clientWidth;
    this.height =
      window.innerHeight ||
      document.documentElement.clientHeight ||
      this.body.clientHeight;

    APP.Modules.forEach(module => {
      module.resize(this.width, this.height);
    });
  }

  modulesInitialize() {
    APP.Modules.forEach((module, i) => {
      document.addEventListener('DOMContentLoaded', () => {
        module.init();
      });
    });
  }
}
export default Main;
