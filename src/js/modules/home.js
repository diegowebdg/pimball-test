/*jshint esversion: 6 */
import BaseComponent from './BaseComponent';

class Home extends BaseComponent {
  constructor(data) {
    super(data);
    this.masonryOptions = {
      columnWidth: 240,
      itemSelector: '.post',
      gutter: 30
    };
  }

  init() {
    this.attachEvents();
  }

  attachEvents() {
    jQuery('.post-list__list').masonry(this.masonryOptions);
    jQuery('.share').on('click', this.share);
  }

  share(e, width, height) {
    e.preventDefault();
    var url = jQuery(this).attr('href'),
      width = 600,
      height = 350,
      wTop = window.screen.top || window.screenTop || 0,
      wLeft = window.screen.left || window.screenLeft || 0,
      left = window.screen.width / 2 - (width / 2 + 10) + wLeft,
      top = window.screen.height / 2 - (height / 2 + 50) + wTop;
    window.open(
      url,
      'popup' + url,
      'status=no, height=' +
        height +
        ', width=' +
        width +
        ', resizable=yes, left=' +
        left +
        ', top=' +
        top +
        ', screenX=' +
        left +
        ', screenY=' +
        top +
        ', toolbar=no, menubar=no, scrollbars=no, location=no, directories=no'
    );
  }
}
export default Home;
